# MapNet
Solving [crowdAI](https://www.crowdai.org/)'s [Mapping Challenge](https://www.crowdai.org/challenges/mapping-challenge/).

Take a look at the [data demonstration](data_demo.ipynb), the [model demonstration](model_demo.ipynb) and finally the [evaluation demonstration](evaluation_demo.ipynb).

At the moment, no actual test score is computed locally. Also, the test set is handled entirely in the [evaluation demonstration](evaluation_demo.ipynb).

## Setup
[Keras](https://keras.io/), [TensorFlow](https://www.tensorflow.org/) and [PyCocoTools](https://github.com/cocodataset/cocoapi/tree/master/PythonAPI/pycocotools) and a few of the usual suspects are are required. Install them however you want, e.g. using [Anaconda](https://anaconda.org/):

```bash
conda create -n mapping python=3.6 tensorflow-gpu keras scikit-image
conda activate mapping
pip install pycocotools
```

Get the data at [crowdai's project page for the challenge](https://www.crowdai.org/challenges/mapping-challenge/dataset_files).

## Training
See `python train.py --help` and train with:

```bash
python train.py --in ~/data/mapping_challenge
```

## Development
Note that the network assumes **channels last**!

## Issues
- the network is fairly weak
- the scaling of the weights is pretty rough, especially w.r.t. the background
- callbacks do not work with the current custom loss as a metric
