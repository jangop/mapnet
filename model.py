import keras


def build_model():
    x = keras.layers.Input(shape=(300, 300, 3), name='input')

    z = keras.layers.BatchNormalization()(x)
    z = keras.layers.Conv2D(64, 4, strides=2, padding='same')(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2D(128, 4, strides=2, padding='same')(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2D(256, 4, strides=2, padding='same')(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2DTranspose(128, 4, strides=2, padding='same')(z)
    z = keras.layers.Cropping2D(((1, 0), (1, 0)))(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2DTranspose(64, 4, strides=2, padding='same')(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2DTranspose(1, 4, strides=2, padding='same')(z)
    y = keras.layers.Activation('sigmoid')(z)

    return keras.models.Model(x, y)


def build_model_with_skips():
    x = keras.layers.Input(shape=(300, 300, 3), name='input')

    z = keras.layers.BatchNormalization()(x)
    c2 = keras.layers.Conv2D(64, 4, strides=2, padding='same')(z)  # 1/2
    z = keras.layers.advanced_activations.LeakyReLU()(c2)
    z = keras.layers.BatchNormalization()(z)
    c4 = keras.layers.Conv2D(128, 4, strides=2, padding='same')(z)  # 1/4
    z = keras.layers.advanced_activations.LeakyReLU()(c4)
    z = keras.layers.BatchNormalization()(z)
    c8 = keras.layers.Conv2D(256, 4, strides=2, padding='same')(z)  # 1/8
    z = keras.layers.advanced_activations.LeakyReLU()(c8)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2D(256, 2, padding='same')(z)  # 1/8
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d4 = keras.layers.Conv2DTranspose(
        128, 4, strides=2, padding='same')(z)  # 1/4
    d4 = keras.layers.Cropping2D(((1, 0), (1, 0)))(d4)
    z = keras.layers.concatenate([d4, c4])
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d2 = keras.layers.Conv2DTranspose(
        64, 4, strides=2, padding='same')(z)  # 1/2
    z = keras.layers.concatenate([d2, c2])
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d1 = keras.layers.Conv2DTranspose(
        1, 4, strides=2, padding='same')(z)  # 1/1
    y = keras.layers.Activation('sigmoid')(d1)

    return keras.models.Model(x, y)


def build_model_with_skips_and_dropout(rate=0.2):
    x = keras.layers.Input(shape=(300, 300, 3), name='input')

    c2 = keras.layers.Conv2D(64, 4, strides=2, padding='same')(x)  # 1/2
    c2 = keras.layers.Dropout(rate)(c2)
    z = keras.layers.advanced_activations.LeakyReLU()(c2)
    c4 = keras.layers.Conv2D(128, 4, strides=2, padding='same')(z)  # 1/4
    c4 = keras.layers.Dropout(rate)(c4)
    z = keras.layers.advanced_activations.LeakyReLU()(c4)
    c8 = keras.layers.Conv2D(256, 4, strides=2, padding='same')(z)  # 1/8
    c8 = keras.layers.Dropout(rate)(c8)
    z = keras.layers.advanced_activations.LeakyReLU()(c8)
    z = keras.layers.Conv2D(256, 2, padding='same')(z)  # 1/8
    z = keras.layers.Dropout(rate)(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d4 = keras.layers.Conv2DTranspose(
        128, 4, strides=2, padding='same')(z)  # 1/4
    d4 = keras.layers.Cropping2D(((1, 0), (1, 0)))(d4)
    z = keras.layers.concatenate([d4, c4])
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d2 = keras.layers.Conv2DTranspose(
        64, 4, strides=2, padding='same')(z)  # 1/2
    z = keras.layers.concatenate([d2, c2])
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d1 = keras.layers.Conv2DTranspose(
        1, 4, strides=2, padding='same')(z)  # 1/1
    y = keras.layers.Activation('sigmoid')(d1)

    return keras.models.Model(x, y)


def build_brimstony_caste(rate=0.2):
    x = keras.layers.Input(shape=(300, 300, 3), name='input')

    c2 = keras.layers.Conv2D(64, 4, padding='same')(x)
    c2 = keras.layers.MaxPooling2D(padding='same')(c2)  # 1/2
    c2 = keras.layers.Dropout(rate)(c2)
    z = keras.layers.advanced_activations.LeakyReLU()(c2)
    c4 = keras.layers.Conv2D(128, 4, padding='same')(z)
    c4 = keras.layers.MaxPooling2D(padding='same')(c4)  # 1/4
    c4 = keras.layers.Dropout(rate)(c4)
    z = keras.layers.advanced_activations.LeakyReLU()(c4)
    c8 = keras.layers.Conv2D(256, 4, padding='same')(z)
    c8 = keras.layers.MaxPooling2D(padding='same')(c8)  # 1/8
    c8 = keras.layers.Dropout(rate)(c8)
    z = keras.layers.advanced_activations.LeakyReLU()(c8)
    z = keras.layers.Conv2D(256, 2, padding='same')(z)  # 1/8
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d4 = keras.layers.Conv2DTranspose(
        128, 4, strides=2, padding='same')(z)  # 1/4
    d4 = keras.layers.Cropping2D(((1, 0), (1, 0)))(d4)
    z = keras.layers.concatenate([d4, c4])
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2D(128, 4, padding='same')(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d2 = keras.layers.Conv2DTranspose(
        64, 4, strides=2, padding='same')(z)  # 1/2
    z = keras.layers.concatenate([d2, c2])
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2D(64, 4, padding='same')(z)
    z = keras.layers.advanced_activations.LeakyReLU()(z)
    z = keras.layers.BatchNormalization()(z)
    d1 = keras.layers.Conv2DTranspose(
        32, 4, strides=2, padding='same')(z)  # 1/1
    z = keras.layers.advanced_activations.LeakyReLU()(d1)
    z = keras.layers.BatchNormalization()(z)
    z = keras.layers.Conv2D(1, 4, padding='same')(z)
    y = keras.layers.Activation('sigmoid')(z)

    return keras.models.Model(x, y)


def weighted_binary_crossentropy(y_true, y_pred):
    y_weight = y_true[..., 1]
    y_weight = keras.backend.expand_dims(y_weight)
    y_true = y_true[..., 0]
    y_true = keras.backend.expand_dims(y_true)
    cross_entropy = keras.backend.binary_crossentropy(y_true, y_pred)
    cross_entropy *= y_weight
    mean = keras.backend.mean(cross_entropy, axis=-1)
    return mean


def binary_accuracy(y_true, y_pred):
    y_true = y_true[..., 0]
    y_true = keras.backend.expand_dims(y_true)
    return keras.backend.binary_accuracy(y_true, y_pred)
