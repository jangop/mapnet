def main():
    import argparse

    # Handle command line arguments.
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--in-path',
        metavar='PATH',
        type=str,
        required=True,
        help=
        'Directory that contains appropriate `train` and `val` subdirectories.'
    )
    parser.add_argument(
        '--epochs',
        metavar='NUM',
        type=int,
        default=100,
        help='Number of epochs to train the model for.')
    parser.add_argument(
        '--channels',
        metavar='NUM',
        type=int,
        default=3,
        help=
        'Number of color channels in the input images. Note that the current architecture only works with 3 color channels per image.'
    )
    parser.add_argument(
        '--batch-size',
        metavar='SIZE',
        type=int,
        default=32,
        help='Number of samples per batch.')
    parser.add_argument(
        '--side',
        metavar='LENGTH',
        type=int,
        default=300,
        help=
        'Side length of input and output images (in pixel). Note that the current architecture only works with image of size 300 by 300.'
    )
    parser.add_argument(
        '--out-path',
        metavar='PATH',
        default=None,
        help=
        'Directory where results are stored. If not set, a default is created based on the current date.'
    )
    parser.add_argument(
        '--small', action='store_true', help='Use only small datasets.')
    parser.add_argument(
        '--weigh',
        action='store_true',
        help='Weigh buildings by inverse area.')
    parser.add_argument(
        '--bg-mode',
        metavar='MODE',
        type=str,
        default='area',
        choices=['constant', 'area', 'minimum'],
        help=
        'How weights are assigned to background pixels. constant: Assign each background pixel a constant weight as determined by the argument --background-value. area: Assign each background pixel a weight based on how many pixels belong to the background. minimum: Clip all weights such that the lowest weight is determined by the argument --background-value. Only relevant if --weigh is set.'
    )
    parser.add_argument(
        '--bg-value',
        metavar='VAL',
        type=float,
        default=1.0,
        help='Weight used as per --background-mode.')
    parser.add_argument(
        '--weight-mode',
        metavar='MODE',
        type=str,
        default='log',
        choices=['log', 'root', 'linear'],
        help=
        'How weights are calculated based on area. log: With a logarithmic function. root: Based on the n-th root. Only relevant if --weigh is set.'
    )
    parser.add_argument(
        '--outline-radius',
        metavar='RADIUS',
        type=int,
        default=5,
        help='Radius for the building outlines.')
    parser.add_argument(
        '--model',
        metavar='MODEL',
        type=str,
        default='skip',
        choices=['simple', 'skip', 'skip-drop', 'brimstony-caste'],
        help='Architecture that is trained.')

    args = parser.parse_args()

    # Import further modules.
    import datetime
    import os
    import pickle

    import keras

    from data import CrowdAIMappingChallenge

    # Select model.
    if args.model == 'simple':
        from model import build_model as build_model
    elif args.model == 'skip':
        from model import build_model_with_skips as build_model
    elif args.model == 'skip-drop':
        from model import build_model_with_skips_and_dropout as build_model
    elif args.model == 'brimstony-caste':
        from model import build_brimstony_caste as build_model

    # Handle out path.
    if args.out_path is None:
        args.out_path = './out-' + datetime.date.today().isoformat()

    os.makedirs(args.out_path, exist_ok=True)

    # Open data.
    TRAIN_IMAGES = 'train/images'
    TRAIN_ANNOTATIONS = 'train/annotation{}.json'
    VAL_IMAGES = 'val/images'
    VAL_ANNOTATIONS = 'val/annotation{}.json'

    train_annotations_path = os.path.join(
        args.in_path, TRAIN_ANNOTATIONS.format('-small' if args.small else ''))
    val_annotations_path = os.path.join(
        args.in_path, VAL_ANNOTATIONS.format('-small' if args.small else ''))

    generator_settings = {
        'batch_size': args.batch_size,
        'side_length': args.side,
        'weigh': args.weigh,
        'background_mode': args.bg_mode,
        'background_value': args.bg_value,
        'weight_mode': args.weight_mode
    }

    train_data = CrowdAIMappingChallenge(
        os.path.join(args.in_path, TRAIN_IMAGES), train_annotations_path,
        **generator_settings)
    val_data = CrowdAIMappingChallenge(
        os.path.join(args.in_path, VAL_IMAGES), val_annotations_path,
        **generator_settings)

    # Set loss.
    if args.weigh:
        from model import weighted_binary_crossentropy
        loss = weighted_binary_crossentropy
    else:
        loss = keras.losses.binary_crossentropy

    # Create model.
    model = build_model()
    model.summary()
    model.compile(loss=loss, optimizer='adam')

    # Prepare training.
    MODEL = 'model.json'
    with open(os.path.join(args.out_path, MODEL), 'w') as handle:
        handle.write(model.to_json())

    ARGS = 'args.txt'
    with open(os.path.join(args.out_path, ARGS), 'w') as handle:
        handle.write(str(args))

    # Prepare callbacks.
    WEIGHTS = 'weights-{epoch:02d}.hdf5'
    save_best_only = not args.weigh
    checkpoint = keras.callbacks.ModelCheckpoint(
        os.path.join(args.out_path, WEIGHTS),
        monitor=loss,
        verbose=1,
        save_best_only=save_best_only,
        mode='min')

    terminate_on_nan = keras.callbacks.TerminateOnNaN()

    early_stopping = keras.callbacks.EarlyStopping(
        monitor=loss, min_delta=0.001, patience=10, verbose=1, mode='min')

    HISTORY = 'history.txt'
    csv_logger = keras.callbacks.CSVLogger(
        os.path.join(args.out_path, HISTORY), separator=' ')

    callbacks = [checkpoint, terminate_on_nan, csv_logger]
    if not args.weigh:
        callbacks += [early_stopping]

    # Train.
    history = model.fit_generator(
        train_data,
        epochs=args.epochs,
        validation_data=val_data,
        callbacks=callbacks)

    # Save history.
    HISTORY = 'history'
    with open(os.path.join(args.out_path, HISTORY), 'wb') as handle:
        pickle.dump(history, handle)


if __name__ == '__main__':
    main()
