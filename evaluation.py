import json
import os

import numpy as np
import scipy.ndimage
import skimage.io
import skimage.morphology

import pycocotools.mask
import utils


class TestGenerator():
    def __init__(self, path, batch_size=16, side_length=300, n_channels=3):
        self._path = path
        self._batch_size = batch_size
        self._side_length = side_length
        self._n_channels = n_channels

        # Collect images.
        self._image_files = os.listdir(self._path)
        self._n_images = len(self._image_files)
        self._n_batches = int(
            np.ceil(self._n_images /
                    self._batch_size))  # Last batch might not be full.

        # Initialize batch counter
        self._i_batch = 0

    def __iter__(self):
        return self

    def __len__(self):
        return self._n_batches

    def __next__(self):
        # Stop when exhausted.
        if self._i_batch >= self._n_batches:
            raise StopIteration()

        # Determine indices w.r.t. batch size.
        i_start = self._i_batch * self._batch_size
        i_end = i_start + self._batch_size
        i_end = min(i_end, self._n_images)

        # Increment batch counter.
        self._i_batch += 1

        # Pick image files and determine respective ids.
        batch_files = self._image_files[i_start:i_end]
        ids = [int(x.replace('.jpg', '')) for x in batch_files]

        # Collect images in batch.
        batch = np.empty((self._batch_size, self._side_length,
                          self._side_length, self._n_channels))
        for i_file, filename in enumerate(batch_files):
            path = os.path.join(self._path, filename)
            image = self._open_image(path)
            batch[i_file, :, :, :] = image

        # Normalize images.
        batch = batch / 127.5 - 1

        return ids, batch

    def _open_image(self, path):
        # Open image.
        image = skimage.io.imread(path)

        return image

    def reset(self):
        self._i_batch = 0




def annotation_from_mask(mask, img_id, category_id=100, score=1):
    mask = np.asfortranarray(mask, dtype=np.uint8)
    rle = pycocotools.mask.encode(mask)
    rle['counts'] = rle['counts'].decode('UTF-8')

    annotation = {}
    annotation['img_id'] = img_id
    annotation['category_id'] = category_id
    annotation['score'] = score
    annotation['segmentation'] = rle
    annotation['bbox'] = pycocotools.mask.toBbox(rle).tolist()

    return annotation


def annotations_from_mask(mask, img_id):
    # Squeeze mask.
    mask = np.squeeze(mask)

    # Find individual buildings.
    parts, n_parts = scipy.ndimage.label(mask)

    # Turn buildings into annotations.
    annotations = []
    for i_part in range(1, n_parts + 1):
        part_mask = (parts == i_part).astype(np.uint8)
        annotations.append(annotation_from_mask(part_mask, img_id))

    return annotations

def post_process_mask(mask, radius=7):
    size = radius * 2 + 1
    kernel = np.ones((size, size), dtype=bool)
    kernel = utils.build_square_binary_kernel(radius)
    mask = skimage.morphology.opening(mask, selem=kernel)
    return mask

def binarize_mask(mask, threshold=0.5):
    mask = mask > threshold
    return mask


def save_annotations(path, annotations):
    with open(path, 'w') as handle:
        handle.write(json.dumps(annotations))


def submit_annotation_file(path, key):
    import crowdai
    challenge = crowdai.Challenge("crowdAIMappingChallenge", key)
    result = challenge.submit(path)
    return result
