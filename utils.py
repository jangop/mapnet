import numpy as np


def build_circular_binary_kernel(radius):
    size = radius * 2 + 1
    kernel = np.zeros((size, size), dtype=bool)
    x, y = np.ogrid[-radius:radius +
                    1, -radius:radius + 1]
    circle = x**2 + y**2 <= radius**2
    kernel[circle] = True
    
    return kernel
    

def build_square_binary_kernel(radius):
    size = radius * 2 + 1
    kernel = np.ones((size, size), dtype=bool)
    
    return kernel
