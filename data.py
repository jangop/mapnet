import os
import random

import numpy as np
import skimage.filters
import skimage.io
import skimage.morphology
import skimage.transform

import keras.utils
import pycocotools.coco
import pycocotools.mask
import utils


class CrowdAIMappingChallenge(keras.utils.Sequence):
    def __init__(self,
                 images_path,
                 annotations_path,
                 side_length=None,
                 batch_size=32,
                 shuffle=True,
                 n_channels=3,
                 weigh=False,
                 background_mode='constant',
                 background_value=1.0,
                 weight_mode='log',
                 outline_radius=5,
                 random_state=None,
                 augment=True):
        self._images_path = images_path
        self._annotations_path = annotations_path
        self._target_side_length = side_length
        self._batch_size = batch_size
        self._shuffle = shuffle
        self._n_channels = n_channels
        self._weigh = weigh
        self._background_mode = background_mode
        self._background_value = background_value
        self._weight_mode = weight_mode
        self._outline_radius = outline_radius
        self._augment = augment

        # Set random state.
        if isinstance(random_state, np.random.RandomState):
            self._random_state = random_state
        else:
            self._random_state = np.random.RandomState(random_state)

        # Determine whether masks are binary.
        self._mask_type = float if self._weigh else bool

        # Set number of channels of masks.
        self._n_channels_mask = 2 if self._weigh else 1

        # Open annotations.
        self._coco = pycocotools.coco.COCO(self._annotations_path)
        self._image_ids = self._coco.getImgIds(catIds=self._coco.getCatIds())

        # See if images and masks need to be scaled.
        self._source_side_length = self._coco.loadImgs(
            self._image_ids[0])[0]['width']
        if self._target_side_length is None:
            self._target_side_length = self._source_side_length
        self._resize = self._target_side_length != self._source_side_length

        # Prepare dilation kernel.
        if self._weigh and self._outline_radius > 0:
            self._dilation_kernel = utils.build_circular_binary_kernel(
                self._outline_radius)

    def __len__(self):
        return int(np.floor(len(self._image_ids) / self._batch_size))

    def __getitem__(self, idx):
        img_ids = self._image_ids[idx * self._batch_size:(idx + 1) *
                                  self._batch_size]
        images = np.empty((self._batch_size, self._target_side_length,
                           self._target_side_length, self._n_channels))
        masks = np.empty((self._batch_size, self._target_side_length,
                          self._target_side_length, self._n_channels_mask))

        for idx_in_batch, img_id in enumerate(img_ids):
            path = os.path.join(self._images_path,
                                self._coco.loadImgs([img_id])[0]['file_name'])
            image = self._open_image(path)

            annotations = self._coco.loadAnns(
                self._coco.getAnnIds(imgIds=img_id))
            mask = self._mask_from_annotations(annotations)

            # Perform data augmentation.
            if self._augment:
                n_rotations = self._random_state.randint(0, 4)
                flip_lr = bool(self._random_state.randint(0, 2))
                flip_ud = bool(self._random_state.randint(0, 2))
                if flip_ud:
                    image = np.flipud(image)
                    mask = np.flipud(mask)
                if flip_lr:
                    image = np.fliplr(image)
                    mask = np.fliplr(mask)
                image = np.rot90(image, k=n_rotations)
                mask = np.rot90(mask, k=n_rotations)

            images[idx_in_batch, :, :, :] = image
            masks[idx_in_batch, :, :, :] = mask

        # Normalize images.
        if self._resize:
            images = images * 2 - 1
        else:
            images = images / 127.5 - 1

        return images, masks

    def on_epoch_end(self):
        # Shuffle.
        if self._shuffle:
            self._random_state.shuffle(self._image_ids)

    def _open_image(self, path):
        # Open image.
        image = skimage.io.imread(path)

        # Resize image.
        if self._resize:
            image = skimage.transform.resize(
                image, (self._target_side_length, self._target_side_length),
                mode='reflect')

        return image

    def _mask_from_annotations(self, annotations):
        # Initialize empty mask.
        mask = np.zeros(
            (self._source_side_length, self._source_side_length,
             self._n_channels_mask),
            dtype=self._mask_type)

        # Paint annotations.
        for anno in annotations:
            rle = pycocotools.mask.frPyObjects(anno['segmentation'],
                                               self._source_side_length,
                                               self._source_side_length)
            part = np.squeeze(pycocotools.mask.decode(rle))

            # Add to mask.
            if self._weigh:
                area = float(pycocotools.mask.area(rle))
                if area > 0.0:
                    part = part.astype(float)
                    weight = self._weight_from_area(
                        area, mode=self._weight_mode)
                    mask[:, :, 1] += part * weight
            mask[:, :, 0] = np.logical_or(mask[:, :, 0], part)

        # Handle background.
        if self._weigh:
            if self._background_mode is 'constant':
                # Assign each background pixel a constant weight.
                background_idx = mask[..., 1] == 0
                mask[background_idx, 1] = self._background_value
            elif self._background_mode is 'area':
                # Assign each background pixel a weight that depends on the total number of background pixels.
                background_area = self._source_side_length * self._source_side_length - np.count_nonzero(
                    mask[..., 0])
                if background_area > 0.0:
                    weight = self._weight_from_area(
                        background_area, mode=self._weight_mode)
                    background_idx = mask[..., 1] == 0
                    mask[background_idx, 1] = weight
            elif self._background_mode is 'minimum':
                # Clip every weight to the range `[background_value, upper]`, where `upper` is `1.0`.
                mask[..., 1] = np.maximum(mask[..., 1], self._background_value)

        # Handle outlines.
        if self._weigh and self._outline_radius > 0:
            dilated = skimage.morphology.binary_dilation(
                mask[:, :, 0], selem=self._dilation_kernel)
            dilated = np.logical_xor(dilated, mask[:, :, 0])

            mask[:, :, 1] = np.maximum(dilated, mask[:, :, 1])

        # Resize mask.
        if self._resize:
            mask = skimage.transform.resize(
                mask, (self._target_side_length, self._target_side_length),
                mode='reflect')

        return mask

    def _weight_from_area(self, area, mode='log', root=16):
        if mode is 'log':
            if area > 0:
                weight = 0.5 / (1 + np.exp(0.001 * (area - 2750))) + 0.5
            else:
                weight = 0.5
        elif mode is 'root':
            if area > 0:
                weight = (np.power(area, -1 / root) + 1) / 2
            else:
                weight = 0.5
        elif mode is 'linear':
            if area > 0:
                weight = 1 / area
                weight = (weight + 1) / 2
            else:
                weight = 0.5

        return weight

    def denormalize(self, image):
        image = ((image + 1) * 127.5).astype(int)

        return image
